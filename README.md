# GMQ210_Projet_Session
Ce projet de session a été réalisé pour le cours GMQ210 par Marie-Hélène Gadbois-Del Carpio, Sarah Pion et Anthony Mandeville. Le code développé offre une analyse des îlots de chaleur de la ville de Montréal. Grâce à une petite interface simple affichée sur la console, on sélectionne quel type d'analyse désirée soit les îlots de chaleur ou les arbres et ensuite il faut sélectionner si on veut analyser un arrondissement en particulier ou toute la ville. Le programme affichera ensuite des cartes et quelques statistiques.


## Création d'environnement
Créer votre environnement avec le fichier  [env_projet_session.yaml](https://gitlab.com/M-H90/gmq210_projet_session/-/blob/main/env_projet_session.yaml)


## Lancement du programme

Nécessite pas de paramètres, simplement cliquer sur le bouton run ou sinon importer comme module le fichier main.py et appeler la fonction main.

