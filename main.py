# -*- coding: utf-8 -*-
# Auteur : Marie-Hélène Gadbois-Del Carpio, Sarah Pion et Anthony Mandeville

import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
from shapely.geometry import Point
from textwrap import wrap


def main():
    # Déclaration des variables
    file_ville = 'limites-administratives-agglomeration-nad83.geojson'
    file_chaleur = 'ilots-de-chaleur-2016.geojson'
    file_arbres = 'arbres-publics.csv'
    crs_choisi = 32188
    convertisseur_km = 10 ** 6
    title1 = 'la ville de Montréal'
    title2 = "l'arrondissement de"

    # Lit les fichiers
    ville_gdf = gpd.read_file(file_ville)
    ilots_chaleur_gdf = gpd.read_file(file_chaleur)
    arbres_df = pd.read_csv(file_arbres)
    # Fichiers des arbres est imcomplet, manque géométrie
    # Créer points à partir des longitudes et latitudes
    points = arbres_df.apply(lambda row: Point(row.Longitude, row.Latitude), axis=1)
    # Créer un nouveau geodataframe à partir du dataframe et des points
    arbres_gdf = gpd.GeoDataFrame(arbres_df, geometry=points)
    # Confirmer la projection
    arbres_gdf.crs = {'init': 'epsg:4326'}

    # Reprojeter géométrie si besoin
    if ville_gdf.crs.to_epsg() != crs_choisi or ilots_chaleur_gdf.crs.to_epsg() != crs_choisi \
            or arbres_gdf.crs.to_epsg() != crs_choisi:
        ville_gdf = ville_gdf.to_crs(crs=crs_choisi)
        ilots_chaleur_gdf = ilots_chaleur_gdf.to_crs(crs=crs_choisi)
        arbres_gdf = arbres_gdf.to_crs(crs=crs_choisi)

    # Le programme roulera tant aussi longtemps on ne choisit pas de quitter
    while True:
        # Message d'accueil et présentation des différents choix
        print("-----------------------------------------------------------------")
        print("Bienvenue au programme d'analyse des îlots de chaleur de Montréal")
        print("-----------------------------------------------------------------")
        print("1-Analyse des îlots de chaleur")
        print("2-Analyse des arbres")
        print("-----------------------------------------------------------------")
        print("Pour quitter le programme: Tapez q")
        print("-----------------------------------------------------------------")
        choice1 = str(input("Entrez votre choix (le numéro ou q):"))
        print("-----------------------------------------------------------------")

        # Si analyse des îlots est choisie
        if choice1 == '1':
            # Choisir type d'analyse voulue
            # Si globale alors masque=ville_gdf sinon masque=arrondissement_gdf
            # title correspond à un string qui précise le nom de l'arrondissement ou la ville entière
            masque, title = choisir_type_analyse(ville_gdf, crs_choisi, title1, title2)

            # Choisir le type d'îlots voulu
            category_ilot = choisir_ilots()
            # Création et affichage du graphique et renvoie le geodataframe de la requête
            response = create_graph(ilots_chaleur_gdf, ville_gdf, masque, category_ilot, crs_choisi, title)
            # Ajouter la surface qu'occupe les îlots ou arbres
            response['Area'] = round(response['geometry'].area / convertisseur_km, 2)

            # Imprimer résultats
            print("-----------------------------------------------------------------")
            print("Résultats d'analyse")
            print("-----------------------------------------------------------------")
            print(f"Superficie calcuée des îlots de la catégorie « {category_ilot} » par arrondissement")
            # Pour chaque rangée du geodataframe
            for i in response.index:
                print(f"{response['NOM'][i]}: {response['Area'][i]} km2")
            print("-----------------------------------------------------------------")
            # Si c'est une analyse globale
            if len(response.index) > 1:
                # Assignation min et max
                min = response[response.area == response.area.min()]
                max = response[response.area == response.area.max()]
                print(f"{max['NOM'].values[0]} est l'arrondissement ayant la plus grande superficie d'îlots de la "
                      f"catégorie « {category_ilot} »: {max['Area'].values[0]} km2")
                print(f"{min['NOM'].values[0]} est l'arrondissement ayant la plus petite superficie d'îlots de la "
                      f"catégorie « {category_ilot} »: {min['Area'].values[0]} km2")
                print(f"En moyenne, la surperficie est de {round(response['Area'].mean(),2)} km2 par arrondissement")

        # Si analyse des arbres est choisie
        elif choice1 == '2':
            # Choisir type d'analyse voulue
            # Si globale alors masque=ville_gdf sinon masque=arrondissement_gdf
            # title correspond à un string qui précise le nom de l'arrondissement ou la ville entière
            masque, title = choisir_type_analyse(ville_gdf, crs_choisi)

            # Création et affichage du graphique et renvoie le geodataframe
            response = create_graph(arbres_gdf, ville_gdf, masque, None, crs_choisi, title)
            # Compter le nombre d'arbres par arrondissement
            response['NB_Arbres'] = 1

            # Imprimer résultats
            print("-----------------------------------------------------------------")
            print("Résultats d'analyse")
            print("-----------------------------------------------------------------")
            print(f"Nombre d'arbres par arrondissement")
            # Réponse aggrégé par arrondissement
            response_par_arr = response.groupby(['NOM']).agg(NB_Arbres=('NB_Arbres', 'sum'))
            print(f"{response_par_arr}")
            print("-----------------------------------------------------------------")
            # Si c'est une analyse globale
            if len(response_par_arr.index) > 1:
                # Assignation min et max
                min = response_par_arr[response_par_arr.NB_Arbres == response_par_arr.NB_Arbres.min()]
                max = response_par_arr[response_par_arr.NB_Arbres == response_par_arr.NB_Arbres.max()]
                print(f"L'arrondissement ayant le plus d'arbres: \n"
                      f"{max} \n")
                print(f"L'arrondissement ayant le moins d'arbres: \n"
                      f"{min} \n")
                print(f"En moyenne, le nombre d'arbres: \n"
                      f"{round(response_par_arr['NB_Arbres'].mean(),2)} arbres")

        # Quitter le programme
        elif choice1 == 'q':
            print("Vous avez quitté le programme")
            break

        # Mauvais choix entré
        else:
            print("-----------------------------------------------------------------")
            print(f"Le choix {choice1} n'existe pas")
            print("-----------------------------------------------------------------")


# Choisir le type d'analyse
def choisir_type_analyse(ville_gdf, crs_choisi, title1, title2):
    # Tant aussi longtemps que l'utilisateur fait un choix qui n'existe pas
    while True:
        print("-----------------------------------------------------------------")
        print("Choissisez quel type d'analyse vous voulez faire")
        print("-----------------------------------------------------------------")
        print("1-Analyse globale")
        print("2-Analyse d'un arrondissement")
        print("-----------------------------------------------------------------")
        choice2 = str(input("Entrez votre choix:"))
        print("-----------------------------------------------------------------")

        # Analyse globale
        if choice2 == '1':
            # Geodataframe de la ville et nom associé pour le graphique
            return ville_gdf, title1

        # Analyse d'arrondissement
        elif choice2 == '2':
            # Tant qu'un arrondissement qui existe n'a pas été choisi
            while True:
                # Choisir arrondissement
                print("-----------------------------------------------------------------")
                arrondissement = str(input("Choissisez l'arrondissement que vous voulez analyser:"))
                print("-----------------------------------------------------------------")
                # Si existe
                if arrondissement in ville_gdf.values:
                    # Création de l'arrondissement
                    arrondissement_gdf = ville_gdf.loc[(ville_gdf['NOM'] == arrondissement), 'CODEID':'geometry']
                    arrondissement_gdf = arrondissement_gdf.to_crs(crs=crs_choisi)
                    return arrondissement_gdf,  f"{title2} {arrondissement}"

                # Sinon
                else:
                    print("-----------------------------------------------------------------")
                    print(f"L'arrondissement {arrondissement} n'existe pas")
                    print("-----------------------------------------------------------------")
        # Sinon
        else:
            print("-----------------------------------------------------------------")
            print(f"Le choix {choice2} n'existe pas")
            print("-----------------------------------------------------------------")


# Choisir le type d'îlots à analyser
def choisir_ilots():
    # Création d'un dictionnaire des différents îlots
    dict_ilots = {'1': "Îlot de fraîcheur", '2': "Température plus froide que la moyenne",
                  '3': "Température proche de la moyenne",
                  '4': "Température plus chaude que la moyenne (à risque)", '5': "Îlot de chaleur"}

    # Tant qu'une des catégories proposées n'a pas été choisie
    while True:
        # Choix de la catégorie d'îlots
        print("-----------------------------------------------------------------")
        print("Choissisez la catégorie d'îlots que vous voulez analysez")
        print("-----------------------------------------------------------------")
        print("1-Îlot de fraîcheur")
        print("2-Température plus froide que la moyenne")
        print("3-Température proche de la moyenne")
        print("4-Température plus chaude que la moyenne (à risque)")
        print("5-Îlot de chaleur")
        print("-----------------------------------------------------------------")
        choice3 = str(input("Entrez la catégorie désirée (le numéro):"))
        print("-----------------------------------------------------------------")

        # L'utilisateur a bien choisi dans les 5 options
        if choice3 in dict_ilots.keys():
            return dict_ilots[choice3]

        # Sinon
        else:
            print("-----------------------------------------------------------------")
            print(f"Le choix {choice3} n'existe pas")
            print("-----------------------------------------------------------------")


# Crééer le graphique et retourner le résultat
def create_graph(data_gdf, ville_gdf, masque, category_ilot, crs_choisi, title):
    # Création du dictionnaire des couleurs selon type îlot
    color_dict = {"Îlot de fraîcheur": "#2c7bb6", "Température plus froide que la moyenne": "#abd9e9",
                  "Température proche de la moyenne": "#ffffbf",
                  "Température plus chaude que la moyenne (à risque)": "#fdae61", "Îlot de chaleur": "#d7191c"
                  }
    # Si on analyse les îlots
    if category_ilot is not None:
        # Sélectionne le bon type d'îlots
        ilots = data_gdf.loc[(data_gdf['Legende'] == category_ilot), 'FID':'geometry']
        ilots = ilots.to_crs(crs=crs_choisi)

        # Exécution de la requête d'intersection entre les ilots et un lieu
        response = gpd.tools.overlay(ilots, masque, how="intersection")
        # Assignation caractéristiques du graphique
        marketsize = 5
        color = color_dict[category_ilot]
        title = f'Répartition des îlots de la catégorie « {category_ilot} » dans {title}'

    # Sinon analyse des arbres
    else:
        # Exécution de la requête d'intersection entre les arbres et un lieu
        response = gpd.tools.overlay(data_gdf, masque, how="intersection")
        # Assignation caractéristiques du graphique
        marketsize = 1
        color = 'green'
        title = f'Répartition des arbres dans {title}'

    # Création de fond de carte
    base = ville_gdf.plot(color='white', edgecolor='black')

    # Création du graphique des îlots dans tout Montréal
    graph_mtl = response.plot(ax=base, marker='o', color=color, markersize=marketsize)
    # Enlever les axes sur le graphique
    graph_mtl.set_axis_off()
    # Afficher le graphique
    plt.title('\n'.join(wrap(title, 60)), fontsize=14, pad='3.0')
    plt.show()

    return response


if __name__ == "__main__":
    main()
